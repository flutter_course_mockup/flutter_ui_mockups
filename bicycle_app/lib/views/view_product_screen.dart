import 'dart:ui';

import 'package:bicycle_app/views/cart_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:gradient_borders/box_borders/gradient_box_border.dart';

class ViewProductScreen extends StatelessWidget {
  const ViewProductScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(
          top: 50,
        ),
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            colors: [
              Color.fromRGBO(36, 44, 59, 1),
              Color.fromRGBO(75, 76, 237, 1),
            ],
            stops: [0.7, 0.5],
          ),
        ),
        child: Stack(
          children: [
            Positioned(
              top: 70,
              left: 300,
              child: SvgPicture.asset(
                "assets/svg/extreme_text.svg",
              ),
            ),
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Row(
                    children: [
                      Text(
                        "GT BIKE",
                        style: GoogleFonts.poppins(
                          fontSize: 30,
                          fontWeight: FontWeight.w500,
                          color: Colors.white,
                        ),
                      ),
                      const Spacer(),
                      Container(
                        padding: const EdgeInsets.all(10),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                          gradient: LinearGradient(
                            begin: Alignment.topLeft,
                            colors: [
                              Color.fromRGBO(55, 182, 233, 1),
                              Color.fromRGBO(72, 92, 236, 1),
                              Color.fromRGBO(75, 76, 237, 1),
                            ],
                          ),
                        ),
                        child: const Center(
                          child: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
                Image.asset("assets/images/detail_screen_bicycle.png"),
                const SizedBox(
                  height: 40,
                ),
                Expanded(
                  child: ClipRRect(
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 5, sigmaY: 6),
                      child: Container(
                          padding: const EdgeInsets.all(20),
                          width: double.infinity,
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(30),
                              topRight: Radius.circular(30),
                            ),
                            color: Colors.white,
                            gradient: LinearGradient(
                              colors: [
                                Color.fromRGBO(53, 63, 84, 0.6),
                                Color.fromRGBO(34, 40, 52, 0.6),
                              ],
                            ),
                          ),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: 120,
                                    padding: const EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                      color:
                                          const Color.fromRGBO(36, 44, 59, 1),
                                      borderRadius: BorderRadius.circular(20),
                                      border: const GradientBoxBorder(
                                        gradient: LinearGradient(
                                          colors: [
                                            Color.fromRGBO(255, 255, 255, 0.5),
                                            Color.fromRGBO(255, 255, 255, 0),
                                          ],
                                        ),
                                      ),
                                    ),
                                    child: Center(
                                      child: Text(
                                        "Description",
                                        style: GoogleFonts.poppins(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: 130,
                                    padding: const EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                      color:
                                          const Color.fromRGBO(72, 92, 236, 1),
                                      borderRadius: BorderRadius.circular(20),
                                      border: const GradientBoxBorder(
                                        gradient: LinearGradient(
                                          colors: [
                                            Color.fromRGBO(255, 255, 255, 0.5),
                                            Color.fromRGBO(255, 255, 255, 0),
                                          ],
                                        ),
                                      ),
                                    ),
                                    child: Center(
                                      child: Text(
                                        "Specification",
                                        style: GoogleFonts.poppins(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 30,
                              ),
                              Text(
                                """Lorem ipsum dolor sit amet. Ab tenetur molestias vel 
rerum cupiditate qui dolores consequatur et 
asperiores sunt ea magnam dolorem qui consectetur 
omnis. Ut error voluptas qui tempora provident aut 
necessitatibus voluptas rem eveniet nulla ut 
accusantium dignissimos aut facilis perspiciatis a 
natus quia.""",
                                style: GoogleFonts.poppins(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white,
                                ),
                              )
                            ],
                          )),
                    ),
                  ),
                )
              ],
            ),
            Positioned(
                top: 830,
                // left: 20,
                child: Container(
                  width: 450,
                  padding: const EdgeInsets.all(15),
                  decoration: const BoxDecoration(
                      color: Color.fromRGBO(36, 44, 59, 1),
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0, -8),
                          color: Color.fromRGBO(0, 0, 0, 0.4),
                          blurRadius: 20,
                        ),
                      ]),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "\$2,599.99",
                        style: GoogleFonts.inter(
                          fontSize: 30,
                          fontWeight: FontWeight.w500,
                          color: Colors.white,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) {
                                return const CartScreen();
                              },
                            ),
                          );
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          height: 70,
                          decoration: const BoxDecoration(
                              gradient: LinearGradient(colors: [
                                Color.fromRGBO(55, 182, 233, 1),
                                Color.fromRGBO(75, 76, 237, 1),
                              ]),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                              border: GradientBoxBorder(
                                  gradient: LinearGradient(
                                colors: [
                                  Color.fromRGBO(255, 251, 251, 0.5),
                                  Color.fromRGBO(255, 251, 251, 0),
                                ],
                              ))),
                          child: Center(
                            child: Text(
                              "Buy Now",
                              style: GoogleFonts.poppins(
                                fontSize: 30,
                                fontWeight: FontWeight.w500,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
