import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CycleCard extends StatelessWidget {
  const CycleCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(20)),
        color: Colors.white,
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(53, 63, 84, 0.6),
            Color.fromRGBO(34, 40, 52, 0.6),
          ],
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            clipBehavior: Clip.none,
            children: [
              Image.asset(
                "assets/images/home_screen_banner_cycle.png",
              ),
              Positioned(
                top: 160,
                child: Text(
                  "Road Bike",
                  style: GoogleFonts.poppins(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: const Color.fromRGBO(195, 195, 195, 1),
                  ),
                ),
              )
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            "Kiross",
            style: GoogleFonts.poppins(
              fontSize: 20,
              fontWeight: FontWeight.w500,
              color: Colors.white,
            ),
          ),
          Text(
            "\$1,599.99",
            style: GoogleFonts.poppins(
              fontSize: 16,
              fontWeight: FontWeight.w500,
              color: const Color.fromRGBO(195, 195, 195, 1),
            ),
          )
        ],
      ),
    );
  }
}
