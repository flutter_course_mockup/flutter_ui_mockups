import 'package:bicycle_app/views/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class OnBoardingScreen extends StatelessWidget {
  const OnBoardingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: const EdgeInsets.only(top: 30, bottom: 30, left: 20),
          width: double.infinity,
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              colors: [
                Color.fromRGBO(36, 44, 59, 1),
                Color.fromRGBO(75, 76, 237, 1),
              ],
              stops: [0.8, 0.5],
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                top: 70,
                left: 300,
                child: SvgPicture.asset(
                  "assets/svg/extreme_text.svg",
                ),
              ),
              Column(
                children: [
                  Image.asset(
                    "assets/images/bicycle_icon.png",
                  ),
                  const Spacer(),
                  Image.asset(
                    "assets/images/onboarding_bicycle.png",
                    fit: BoxFit.cover,
                  ),
                  const Spacer(),
                  Align(
                    alignment: Alignment.centerRight,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) {
                              return const HomeScreen();
                            },
                          ),
                        );
                      },
                      child: Container(
                        width: 240,
                        padding: const EdgeInsets.all(9),
                        decoration: const BoxDecoration(
                            color: Color.fromRGBO(36, 44, 59, 1),
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(30),
                              bottomLeft: Radius.circular(30),
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.25),
                                offset: Offset(-3, 4),
                                blurRadius: 4,
                              )
                            ]),
                        child: Row(
                          children: [
                            Container(
                              padding: const EdgeInsets.all(15),
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color.fromRGBO(75, 76, 237, 1),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color.fromRGBO(239, 186, 51, 1),
                                      blurRadius: 3,
                                      spreadRadius: 5)
                                ],
                              ),
                              child: const Center(
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 30,
                            ),
                            Text(
                              "Get Started",
                              style: GoogleFonts.poppins(
                                fontSize: 25,
                                fontWeight: FontWeight.w500,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
