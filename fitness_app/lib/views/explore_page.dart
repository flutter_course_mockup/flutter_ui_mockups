import 'package:fitness_app/views/widgets/best_for_you_card.dart';
import 'package:fitness_app/views/widgets/warmup_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class ExploreScreen extends StatelessWidget {
  const ExploreScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.only(
              top: 30,
              left: 20,
              right: 20,
            ),
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    Container(
                      foregroundDecoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        gradient: LinearGradient(
                          stops: [0.3, 0.7],
                          colors: [
                            Color.fromRGBO(0, 0, 0, 0.51),
                            Color.fromRGBO(0, 0, 0, 0),
                          ],
                        ),
                      ),
                      child: Image.asset(
                        "assets/images/explore_page_banner_image.png",
                        height: 200,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 200,
                            child: Text(
                              "Best Quarantine Workout",
                              style: GoogleFonts.lato(
                                fontSize: 25,
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 50,
                          ),
                          Row(
                            children: [
                              Text(
                                "See More",
                                style: GoogleFonts.lato(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w700,
                                  color: const Color.fromRGBO(187, 242, 70, 1),
                                ),
                              ),
                              const Icon(
                                Icons.keyboard_arrow_right,
                                color: Color.fromRGBO(187, 242, 70, 1),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  "Best for you",
                  style: GoogleFonts.lato(
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                SizedBox(
                  height: 250,
                  child: GridView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 1 / 2,
                    ),
                    itemBuilder: (context, index) {
                      return const BestForYouCard();
                    },
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  "Challenge",
                  style: GoogleFonts.lato(
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Stack(
                      children: [
                        Container(
                          height: 120,
                          width: 120,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(16),
                            color: const Color.fromRGBO(187, 242, 70, 1),
                          ),
                        ),
                        Positioned(
                          top: 40,
                          left: 55,
                          child: SvgPicture.asset(
                            "assets/svg/plank_challange_icon.svg",
                          ),
                        ),
                        Positioned(
                            top: 60,
                            left: 10,
                            child: SizedBox(
                              width: 70,
                              child: Text(
                                "Plank Challenge",
                                style: GoogleFonts.lato(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ))
                      ],
                    ),
                    Stack(
                      children: [
                        Container(
                          height: 120,
                          width: 120,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(16),
                            color: const Color.fromRGBO(25, 33, 38, 1),
                          ),
                        ),
                        Positioned(
                          top: 40,
                          left: 50,
                          child: SvgPicture.asset(
                            "assets/svg/sprint_challange_icon.svg",
                          ),
                        ),
                        Positioned(
                            top: 60,
                            left: 10,
                            child: SizedBox(
                              width: 70,
                              child: Text(
                                "Sprint Challenge",
                                style: GoogleFonts.lato(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white,
                                ),
                              ),
                            ))
                      ],
                    ),
                    Stack(
                      children: [
                        Container(
                          height: 120,
                          width: 120,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(16),
                          ),
                        ),
                        Positioned(
                          top: 40,
                          left: 55,
                          child: SvgPicture.asset(
                            "assets/svg/squat_challange_icon.svg",
                          ),
                        ),
                        Positioned(
                            top: 60,
                            left: 10,
                            child: SizedBox(
                              width: 70,
                              child: Text(
                                "Squat Challenge",
                                style: GoogleFonts.lato(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ))
                      ],
                    )
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  "Fast Warmup",
                  style: GoogleFonts.lato(
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                SizedBox(
                  height: 110,
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return const WarmupCard();
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
