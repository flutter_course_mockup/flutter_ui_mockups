import 'package:awesome_bottom_bar/awesome_bottom_bar.dart';
import 'package:fitness_app/views/activity_page.dart';
import 'package:fitness_app/views/explore_page.dart';
import 'package:fitness_app/views/home_screen.dart';
import 'package:flutter/material.dart';

class BottomnavigationScreen extends StatefulWidget {
  const BottomnavigationScreen({super.key});

  @override
  State<BottomnavigationScreen> createState() => _BottomnavigationScreenState();
}

class _BottomnavigationScreenState extends State<BottomnavigationScreen> {
  List<Widget> screenList = [
    const HomeScreen(),
    const ExploreScreen(),
    const ActivityScreen()
  ];
  int currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screenList[currentIndex],
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(15),
        child: BottomBarSalomon(
          onTap: (index) {
            setState(() {
              currentIndex = index;
            });
          },
          borderRadius: const BorderRadius.all(
            Radius.circular(30),
          ),
          items: const [
            TabItem(icon: Icons.home_outlined, title: "Home", key: "Home"),
            TabItem(
                icon: Icons.explore_outlined, title: "Explore", key: "Explore"),
            TabItem(
                icon: Icons.analytics_outlined,
                title: "Analytics",
                key: "Analytics"),
          ],
          colorSelected: Colors.black,
          color: Colors.white,
          indexSelected: 1,
          backgroundColor: Colors.black,
          backgroundSelected: const Color.fromRGBO(187, 242, 70, 1),
        ),
      ),
    );
  }
}
