import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TodaysPlanCard extends StatelessWidget {
  const TodaysPlanCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(9),
            child: Image.asset(
              "assets/images/push_up_image.png",
            ),
          ),
          const SizedBox(width: 10),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                width: 100,
                height: 25,
                decoration: const BoxDecoration(
                  color: Color.fromRGBO(25, 33, 38, 1),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
                ),
                child: Center(
                  child: Text(
                    "Intermidiate",
                    style: GoogleFonts.lato(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Colors.white),
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Push Up",
                    style: GoogleFonts.lato(
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  const SizedBox(
                    height: 6,
                  ),
                  Text(
                    "100 Push up a day",
                    style: GoogleFonts.lato(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(25, 33, 38, 0.5),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Stack(
                    children: [
                      SizedBox(
                        width: 250,
                        height: 20,
                        child: LinearProgressIndicator(
                          backgroundColor: Color.fromRGBO(242, 242, 242, 1),
                          value: 0.4,
                          semanticsValue: "10",
                          color: Color.fromRGBO(187, 242, 70, 1),
                          borderRadius: BorderRadius.all(
                            Radius.circular(5),
                          ),
                        ),
                      ),
                      Positioned(
                        left: 30,
                        child: Text("45 %"),
                      )
                    ],
                  )
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
