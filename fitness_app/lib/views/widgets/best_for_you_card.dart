import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class BestForYouCard extends StatelessWidget {
  const BestForYouCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Image.asset(
          "assets/images/belly_fat_burner.png",
          fit: BoxFit.cover,
          height: 80,
        ),
        const SizedBox(
          width: 10,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Belly fat burner",
              style: GoogleFonts.lato(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                color: const Color.fromRGBO(25, 33, 38, 1),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(6),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: const Color.fromRGBO(241, 241, 241, 1),
              ),
              child: Text(
                "10 min",
                style: GoogleFonts.lato(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: const Color.fromRGBO(25, 33, 38, 0.7),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(6),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: const Color.fromRGBO(241, 241, 241, 1),
              ),
              child: Text(
                "Beginner",
                style: GoogleFonts.lato(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: const Color.fromRGBO(25, 33, 38, 0.7),
                ),
              ),
            )
          ],
        )
      ],
    );
  }
}
