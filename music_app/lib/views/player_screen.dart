import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class PlayerScreen extends StatelessWidget {
  const PlayerScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Column(
        children: [
          Stack(
            children: [
              Container(
                foregroundDecoration: const BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0.7, 1],
                    colors: [
                      Colors.transparent,
                      Colors.black,
                    ],
                  ),
                ),
                child: Image.network(
                  "https://s3-alpha-sig.figma.com/img/d519/2450/a5278e328a4c0baaf9e30c8a1e782b06?Expires=1717977600&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Z7JpOp61FghyrdFAR~UnnylKlHKrH2x0TwFoL~iYIr5KM~xJb0Wvr1RC8Tz4rQPsx6WKtE-AD-rmykeefX8rfBLt-2ez8fSSAYvzNBjgu45U~36SIpgdLp5psRh-3bXHIA~vktw9j7RJFHNlAe~c48VkPIT4P6gx~IcRn6KQ9uw8M-SDqhiIwAv5yeSQf10Bj54PHpS2H4zw2dY6Us3o7QwJQjXzlkCu3SWa8zFtLH7208Eezv5sXIB4qsWidvkI61c5b~zVaA3LJw0TK0M5b-g5CNGjB2mlYnv2JaLzfPm37Q5YP66f3ib4Ag5G5gdDU9NadRtTf6uWsQpK7jP2EA__",
                  height: 650,
                  width: double.infinity,
                  fit: BoxFit.cover,
                ),
              ),
              Positioned(
                top: 500,
                left: 130,
                child: Text(
                  "Alone in the Abyss",
                  style: GoogleFonts.poppins(
                    fontSize: 24,
                    fontWeight: FontWeight.w400,
                    color: const Color.fromRGBO(230, 154, 21, 1),
                  ),
                ),
              ),
              Positioned(
                top: 535,
                left: 200,
                child: Text(
                  "Youlakou",
                  style: GoogleFonts.poppins(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
              ),
              Positioned(
                top: 560,
                left: 400,
                child: SvgPicture.asset(
                  "assets/svg/share_icon.svg",
                  color: Color.fromRGBO(230, 154, 21, 1),
                  height: 25,
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Dynamic Warmup | ",
                  style: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                  ),
                ),
                Text(
                  "4 min",
                  style: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                  ),
                )
              ],
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: LinearProgressIndicator(
              value: 0.4,
              minHeight: 7,
              borderRadius: BorderRadius.all(Radius.circular(20)),
              backgroundColor: Color.fromRGBO(217, 217, 217, 0.19),
              color: Color.fromRGBO(230, 154, 21, 1),
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          const Row(
            children: [],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SvgPicture.asset(
                "assets/svg/replay_icon.svg",
                height: 30,
              ),
              const Icon(
                Icons.skip_previous,
                color: Colors.white,
                size: 35,
              ),
              const CircleAvatar(
                radius: 35,
                child: Icon(
                  Icons.play_arrow,
                  size: 50,
                ),
              ),
              const Icon(
                Icons.skip_previous,
                color: Colors.white,
                size: 35,
              ),
              const Icon(
                Icons.volume_up,
                color: Colors.white,
                size: 35,
              )
            ],
          )
        ],
      ),
    );
  }
}
