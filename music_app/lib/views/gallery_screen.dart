import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:music_app/views/player_screen.dart';

class GalleryScreen extends StatelessWidget {
  const GalleryScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.black,
          selectedItemColor: const Color.fromRGBO(230, 154, 21, 1),
          unselectedItemColor: const Color.fromRGBO(157, 178, 206, 1),
          showSelectedLabels: true,
          showUnselectedLabels: true,
          items: [
            const BottomNavigationBarItem(
              backgroundColor: Colors.black,
              icon: Icon(Icons.favorite_outline),
              label: "Favourite",
            ),
            const BottomNavigationBarItem(
              icon: Icon(Icons.search_outlined),
              label: "Search",
            ),
            BottomNavigationBarItem(
              icon: SvgPicture.asset("assets/svg/home_icon.svg"),
              label: "Home",
            ),
            BottomNavigationBarItem(
              icon: SvgPicture.asset("assets/svg/cart.svg"),
              label: "Cart",
            ),
            const BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: "User",
            ),
          ],
        ),
        backgroundColor: Colors.black,
        body: SingleChildScrollView(
          child: Column(
            children: [
              Stack(
                children: [
                  Container(
                    foregroundDecoration: const BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        stops: [0.7, 1],
                        colors: [
                          Colors.transparent,
                          Colors.black,
                        ],
                      ),
                    ),
                    child: Image.asset(
                      "assets/images/galary_backgroung_image.png",
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Positioned(
                    top: 250,
                    left: 30,
                    child: Text(
                      "A.L.O.N.E",
                      style: GoogleFonts.poppins(
                        fontSize: 39,
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 310,
                    left: 30,
                    child: Container(
                      height: 45,
                      width: 150,
                      decoration: BoxDecoration(
                        color: const Color.fromRGBO(255, 46, 0, 1),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Center(
                        child: Text(
                          "Subscribe",
                          style: GoogleFonts.poppins(
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: 25,
                    height: 8,
                    decoration: const BoxDecoration(
                        color: Color.fromRGBO(255, 61, 0, 1),
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                  ),
                  const SizedBox(width: 4),
                  const CircleAvatar(
                    radius: 5,
                    backgroundColor: Color.fromRGBO(159, 159, 159, 1),
                  ),
                  const SizedBox(width: 4),
                  const CircleAvatar(
                    radius: 5,
                    backgroundColor: Color.fromRGBO(159, 159, 159, 1),
                  ),
                ],
              ),
              const SizedBox(height: 15),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Discography",
                      style: GoogleFonts.poppins(
                        fontSize: 17,
                        fontWeight: FontWeight.w600,
                        color: const Color.fromRGBO(255, 46, 0, 1),
                      ),
                    ),
                    Text(
                      "See All",
                      style: GoogleFonts.poppins(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: const Color.fromRGBO(248, 162, 69, 1),
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(height: 15),
              SizedBox(
                height: 200,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.only(right: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Image.asset(
                                  "assets/images/dead_inside_image.png"),
                              const SizedBox(height: 15),
                              Text(
                                "Dead inside",
                                style: GoogleFonts.poppins(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600,
                                    color:
                                        const Color.fromRGBO(203, 200, 200, 1)),
                              ),
                              Text(
                                "2023",
                                style: GoogleFonts.poppins(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                  color: const Color.fromRGBO(132, 125, 125, 1),
                                ),
                              )
                            ],
                          ),
                        );
                      }),
                ),
              ),
              const SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Popular Singles",
                      style: GoogleFonts.poppins(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        color: const Color.fromRGBO(203, 200, 200, 1),
                      ),
                    ),
                    Text(
                      "See All",
                      style: GoogleFonts.poppins(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: const Color.fromRGBO(248, 162, 69, 1),
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: ListView.builder(
                  itemCount: 10,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        ///ROUTE TO THE PLAYER SCREEN
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) {
                              return const PlayerScreen();
                            },
                          ),
                        );
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: Row(
                          children: [
                            Image.asset("assets/images/we_are_chaos_image.png"),
                            const SizedBox(
                              width: 15,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'We Are Chaos',
                                  style: GoogleFonts.poppins(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      color: const Color.fromRGBO(
                                          203, 200, 200, 1)),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  '2023 * Easy Living',
                                  style: GoogleFonts.poppins(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    color:
                                        const Color.fromRGBO(132, 125, 125, 1),
                                  ),
                                )
                              ],
                            ),
                            const Spacer(),
                            const Center(
                              child: Icon(
                                Icons.more_vert,
                                color: Color.fromRGBO(217, 217, 217, 1),
                                size: 40,
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
